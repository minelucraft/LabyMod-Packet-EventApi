package de.minelucraft.rewinsideAddon.eventsystem.event;

import de.minelucraft.rewinsideAddon.eventsystem.utilities.Event;
import net.minecraft.network.play.server.S05PacketSpawnPosition;
import net.minecraft.util.BlockPos;

public class SpawnPositionEvent extends Event {

    private S05PacketSpawnPosition packet;

    public SpawnPositionEvent(S05PacketSpawnPosition packet) {
        super("SpawnPositionEvent");
        this.packet = packet;
    }

    public S05PacketSpawnPosition getAsPacket() { return this.packet; }

    public BlockPos getBlockPos() { return this.packet.getSpawnPos(); }

}
