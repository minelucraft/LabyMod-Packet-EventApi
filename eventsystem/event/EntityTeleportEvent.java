package de.minelucraft.rewinsideAddon.eventsystem.event;

import de.minelucraft.rewinsideAddon.eventsystem.utilities.Event;
import net.minecraft.network.play.server.S18PacketEntityTeleport;
import net.minecraft.util.BlockPos;

public class EntityTeleportEvent extends Event {

    private S18PacketEntityTeleport packet;

    public EntityTeleportEvent(S18PacketEntityTeleport packet) {
        super("EntityTeleportEvent");
        this.packet = packet;
    }

    public S18PacketEntityTeleport getAsPacket() { return this.packet; }

    public boolean isEntityOnGround() { return this.packet.getOnGround(); }

    public int getEntityId() { return this.packet.getEntityId(); }

    public float getPitch() { return this.packet.getPitch(); }

    public float getYaw() { return this.packet.getYaw(); }

    public double getX() { return this.packet.getX(); }

    public double getY() { return this.packet.getY(); }

    public double getZ() { return this.packet.getZ(); }

    public BlockPos getBlockPos() { return new BlockPos(this.getX(), this.getY(), this.getZ()); }

}
