package de.minelucraft.rewinsideAddon.eventsystem.event;

import de.minelucraft.rewinsideAddon.eventsystem.utilities.Event;
import net.minecraft.network.play.server.S39PacketPlayerAbilities;

public class PlayerAbilitiesEvent extends Event {

    private S39PacketPlayerAbilities packet;

    public PlayerAbilitiesEvent(S39PacketPlayerAbilities packet) {
        super("PlayerAbilitiesEvent");
        this.packet = packet;
    }

    public S39PacketPlayerAbilities getAsPacket() { return this.packet; }

    public boolean isInvulnerable() { return this.packet.isInvulnerable(); }

    public void setInvulnerable(boolean isInvulnerable) { this.packet.setInvulnerable(isInvulnerable); }

    public boolean isFlying() { return this.packet.isFlying(); }

    public void setFlying(boolean isFlying) { this.packet.setFlying(isFlying); }

    public boolean isAllowFlying() { return this.packet.isAllowFlying(); }

    public void setAllowFlying(boolean isAllowFlying) { this.packet.setAllowFlying(isAllowFlying); }

    public boolean isCreativeMode() { return this.packet.isCreativeMode(); }

    public void setCreativeMode(boolean isCreativeMode) { this.packet.setCreativeMode(isCreativeMode); }

    public float getFlySpeed() { return this.packet.getFlySpeed(); }

    public void setFlySpeed(float flySpeedIn) { this.packet.setFlySpeed(flySpeedIn); }

    public float getWalkSpeed() { return this.packet.getWalkSpeed(); }

    public void setWalkSpeed(float walkSpeedIn) { this.packet.setWalkSpeed(walkSpeedIn); }

}
