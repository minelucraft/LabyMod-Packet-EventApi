package de.minelucraft.rewinsideAddon.eventsystem.event;

import de.minelucraft.rewinsideAddon.eventsystem.utilities.Event;
import net.minecraft.network.play.server.S45PacketTitle;
import net.minecraft.util.IChatComponent;

public class TitleEvent extends Event {

    private S45PacketTitle titlePacket;

    public TitleEvent(S45PacketTitle titlePacket) {
        super("TitleEvent");
        this.titlePacket = titlePacket;
    }

    public IChatComponent getTitle() { return this.titlePacket.getMessage(); }

    public S45PacketTitle.Type getType() { return this.titlePacket.getType(); }

    public int getFadeIn() { return this.titlePacket.getFadeInTime(); }

    public int getDisplayTime() { return this.titlePacket.getDisplayTime(); }

    public int getFadeOut() { return this.titlePacket.getFadeOutTime(); }

    public S45PacketTitle getTitleAsPacket() { return this.titlePacket; }

}
