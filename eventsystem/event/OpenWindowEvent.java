package de.minelucraft.rewinsideAddon.eventsystem.event;

import de.minelucraft.rewinsideAddon.eventsystem.utilities.Event;
import net.minecraft.network.play.server.S2DPacketOpenWindow;
import net.minecraft.util.IChatComponent;

public class OpenWindowEvent extends Event {

    private S2DPacketOpenWindow packet;

    public OpenWindowEvent(S2DPacketOpenWindow packet) {
        super("OpenWindowEvent");
        this.packet = packet;
    }

    public S2DPacketOpenWindow getAsPacket() { return this.packet; }

    public int getWindowId() { return this.packet.getWindowId(); }

    public String getGuiId() { return this.packet.getGuiId(); }

    public int getEntityId() { return this.packet.getEntityId(); }

    public int getSlotCount() { return this.packet.getSlotCount(); }

    public IChatComponent getWindowTitle() { return this.packet.getWindowTitle(); }

    public boolean hasSlots() { return this.packet.hasSlots(); }

}
