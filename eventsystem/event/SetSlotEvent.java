package de.minelucraft.rewinsideAddon.eventsystem.event;

import de.minelucraft.rewinsideAddon.eventsystem.utilities.Event;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.server.S2FPacketSetSlot;

public class SetSlotEvent extends Event {

    private S2FPacketSetSlot packet;

    public SetSlotEvent(S2FPacketSetSlot packet) {
        super("SetSlotEvent");
        this.packet = packet;
    }

    public S2FPacketSetSlot getAsPacket() { return this.packet; }

    public ItemStack getItemStack() { return this.packet.func_149174_e(); }

    public int getSlot() { return this.packet.func_149173_d(); }

    public int getWindowId() { return this.packet.func_149175_c(); }

}
