package de.minelucraft.rewinsideAddon.eventsystem.event;

import de.minelucraft.rewinsideAddon.eventsystem.utilities.Event;
import net.minecraft.network.play.server.S33PacketUpdateSign;
import net.minecraft.util.BlockPos;
import net.minecraft.util.IChatComponent;

public class UpdateSignEvent extends Event {

    private S33PacketUpdateSign packet;

    public UpdateSignEvent(S33PacketUpdateSign packet) {
        super("UpdateSignEvent");
        this.packet = packet;
    }

    public S33PacketUpdateSign getAsPacket() { return this.packet; }

    public BlockPos getLocation() { return this.packet.getPos(); }

    public IChatComponent[] getLines() { return this.packet.getLines(); }

}
