package de.minelucraft.rewinsideAddon.eventsystem.event;

import de.minelucraft.rewinsideAddon.eventsystem.utilities.Event;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.util.BlockPos;

public class UpdateTileEntityEvent extends Event {

    private S35PacketUpdateTileEntity packet;

    public UpdateTileEntityEvent(S35PacketUpdateTileEntity packet) {
        super("UpdateTileEntityEvent");
        this.packet = packet;
    }

    public S35PacketUpdateTileEntity getAsPacket() { return this.packet; }

    public BlockPos getLocation() { return this.packet.getPos(); }

    public int getType() { return this.packet.getTileEntityType(); }

    public NBTTagCompound getNBTTag() { return this.packet.getNbtCompound(); }

}
