package de.minelucraft.rewinsideAddon.eventsystem.event;

import de.minelucraft.rewinsideAddon.eventsystem.utilities.Event;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.server.S30PacketWindowItems;

public class WindowItemsEvent extends Event {

    private S30PacketWindowItems packet;

    public WindowItemsEvent(S30PacketWindowItems packet) {
        super("WindowItemsEvent");
        this.packet = packet;
    }

    public S30PacketWindowItems getAsPacket() { return this.packet; }

    public int getWindowId() { return this.packet.func_148911_c(); }

    public ItemStack[] getItemStack() { return this.packet.getItemStacks(); }

}
