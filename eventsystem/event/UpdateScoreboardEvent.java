package de.minelucraft.rewinsideAddon.eventsystem.event;

import de.minelucraft.rewinsideAddon.eventsystem.utilities.Event;
import net.minecraft.network.play.server.S3CPacketUpdateScore;

public class UpdateScoreboardEvent extends Event {

    private S3CPacketUpdateScore packet;

    public UpdateScoreboardEvent(S3CPacketUpdateScore packet) {
        super("UpdateScoreboardEvent");
        this.packet = packet;
    }

    public S3CPacketUpdateScore getAsPacket() { return this.packet; }

    public String getObjectiveName() { return this.packet.getObjectiveName(); }

    public String getPlayerName() { return this.packet.getPlayerName(); }

    public S3CPacketUpdateScore.Action getAction() { return this.packet.getScoreAction(); }

    public int getValue() { return this.packet.getScoreValue(); }

}
