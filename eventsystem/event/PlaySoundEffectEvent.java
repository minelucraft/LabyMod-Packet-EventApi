package de.minelucraft.rewinsideAddon.eventsystem.event;

import de.minelucraft.rewinsideAddon.eventsystem.utilities.Event;
import net.minecraft.network.play.server.S29PacketSoundEffect;

public class PlaySoundEffectEvent extends Event {

    private S29PacketSoundEffect packet;

    public PlaySoundEffectEvent(S29PacketSoundEffect packet) {
        super("PlaySoundEffectEvent");
        this.packet = packet;
    }

    public S29PacketSoundEffect getAsPacket() { return this.packet; }

    public String getSoundName() { return this.packet.getSoundName(); }

    public float getSoundVolume() { return this.packet.getVolume(); }

    public float getSoundPitch() { return this.packet.getPitch(); }

    public double getSoundLocationX() { return this.packet.getX(); }

    public double getSoundLocationY() { return this.packet.getY(); }

    public double getSoundLocationZ() { return this.packet.getZ(); }

}
