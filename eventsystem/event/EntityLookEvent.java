package de.minelucraft.rewinsideAddon.eventsystem.event;

import de.minelucraft.rewinsideAddon.eventsystem.utilities.Event;
import net.minecraft.network.play.server.S14PacketEntity;
import net.minecraft.util.BlockPos;

public class EntityLookEvent extends Event {

    private S14PacketEntity.S16PacketEntityLook packet;

    public EntityLookEvent(S14PacketEntity.S16PacketEntityLook packet) {
        super("EntityLookEvent");
        this.packet = packet;
    }

    public S14PacketEntity.S16PacketEntityLook getAsPacket() { return this.packet; }

    public boolean isEntityOnGround() { return this.packet.getOnGround(); }

    public float getPitch() { return this.packet.func_149063_g(); }

    public float getYaw() { return this.packet.func_149066_f(); }

    public byte getX() { return this.packet.func_149062_c(); }

    public byte getY() { return this.packet.func_149061_d(); }

    public byte getZ() { return this.packet.func_149064_e(); }

    public BlockPos getBlockPos() { return new BlockPos(this.getX(), this.getY(), this.getZ()); }

}
