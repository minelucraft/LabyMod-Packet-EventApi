package de.minelucraft.rewinsideAddon.eventsystem.event;

import de.minelucraft.rewinsideAddon.eventsystem.utilities.Event;
import net.minecraft.network.PacketBuffer;
import net.minecraft.network.play.server.S3FPacketCustomPayload;

public class CustomPayloadEvent extends Event {

    private S3FPacketCustomPayload packet;

    public CustomPayloadEvent(S3FPacketCustomPayload packet) {
        super("CustomPayloadEvent");
        this.packet = packet;
    }

    public S3FPacketCustomPayload getAsPacket() { return this.packet; }

    public String getChannelName() { return this.packet.getChannelName(); }

    public PacketBuffer getPacketBuffer() { return this.packet.getBufferData(); }

}
