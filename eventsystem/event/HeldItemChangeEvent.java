package de.minelucraft.rewinsideAddon.eventsystem.event;

import de.minelucraft.rewinsideAddon.eventsystem.utilities.Event;
import net.minecraft.network.play.server.S09PacketHeldItemChange;

public class HeldItemChangeEvent extends Event {

    private S09PacketHeldItemChange packet;

    public HeldItemChangeEvent(S09PacketHeldItemChange packet) {
        super("HeldItemChangeEvent");
        this.packet = packet;
    }

    public S09PacketHeldItemChange getAsPacket() { return this.packet; }

    public int getSlot() { return this.packet.getHeldItemHotbarIndex(); }

}
