package de.minelucraft.rewinsideAddon.eventsystem.asm;

import de.minelucraft.rewinsideAddon.addon.RewinsideAddon;
import de.minelucraft.rewinsideAddon.eventsystem.event.*;
import de.minelucraft.rewinsideAddon.eventsystem.utilities.EventManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.*;

public class PacketListener {

    private Packet lastPacket;
    private EventManager eventManager;

    public PacketListener() { this.eventManager = RewinsideAddon.getManager().getEventManager(); }

    public void handleIncomingPacket(Packet packet) {
        this.lastPacket = packet;

        if (packet instanceof S3FPacketCustomPayload) {
            this.getEventManager().callEvent(new CustomPayloadEvent((S3FPacketCustomPayload) packet));
            return;
        }

        if (packet instanceof S14PacketEntity.S16PacketEntityLook) {
            this.getEventManager().callEvent(new EntityLookEvent((S14PacketEntity.S16PacketEntityLook) packet));
            return;
        }

        if (packet instanceof S18PacketEntityTeleport) {
            this.getEventManager().callEvent(new EntityTeleportEvent((S18PacketEntityTeleport) packet));
            return;
        }

        if (packet instanceof S09PacketHeldItemChange) {
            this.getEventManager().callEvent(new HeldItemChangeEvent((S09PacketHeldItemChange) packet));
            return;
        }

        if (packet instanceof S2DPacketOpenWindow) {
            this.getEventManager().callEvent(new OpenWindowEvent((S2DPacketOpenWindow) packet));
            return;
        }

        if (packet instanceof S39PacketPlayerAbilities) {
            this.getEventManager().callEvent(new PlayerAbilitiesEvent((S39PacketPlayerAbilities) packet));
            return;
        }

        if (packet instanceof S29PacketSoundEffect) {
            this.getEventManager().callEvent(new PlaySoundEffectEvent((S29PacketSoundEffect) packet));
            return;
        }

        if (packet instanceof S2FPacketSetSlot) {
            this.getEventManager().callEvent(new SetSlotEvent((S2FPacketSetSlot) packet));
            return;
        }

        if (packet instanceof S05PacketSpawnPosition) {
            this.getEventManager().callEvent(new SpawnPositionEvent((S05PacketSpawnPosition) packet));
            return;
        }

        if (packet instanceof S45PacketTitle) {
            this.getEventManager().callEvent(new TitleEvent((S45PacketTitle) packet));
            return;
        }

        if (packet instanceof S3CPacketUpdateScore) {
            this.getEventManager().callEvent(new UpdateScoreboardEvent((S3CPacketUpdateScore) packet));
            return;
        }

        if (packet instanceof S33PacketUpdateSign) {
            this.getEventManager().callEvent(new UpdateSignEvent((S33PacketUpdateSign) packet));
            return;
        }

        if (packet instanceof UpdateTileEntityEvent) {
            this.getEventManager().callEvent(new UpdateTileEntityEvent((S35PacketUpdateTileEntity) packet));
            return;
        }

        if (packet instanceof S30PacketWindowItems) {
            this.getEventManager().callEvent(new WindowItemsEvent((S30PacketWindowItems) packet));
            return;
        }

        System.out.println(packet.getClass().getSimpleName());

    }

    public Packet getLastPacket() { return this.lastPacket; }

    public EventManager getEventManager() { return this.eventManager; }
}
