package de.minelucraft.rewinsideAddon.eventsystem.asm;

import de.minelucraft.rewinsideAddon.addon.RewinsideAddon;
import net.minecraft.network.Packet;

public class Methods {

    public Methods() { }

    public static boolean packet(Packet packetIn) {
        if(packetIn == null)
            return false;
        RewinsideAddon.getManager().getPacketListener().handleIncomingPacket(packetIn);
        return true;
    }

}
