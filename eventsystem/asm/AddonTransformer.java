package de.minelucraft.rewinsideAddon.eventsystem.asm;

import net.minecraft.launchwrapper.IClassTransformer;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.tree.*;

import java.util.Iterator;

public class AddonTransformer implements IClassTransformer {

    /*
     * (!) Das ist nicht selbst gecodet, ist eine leicht abgeänderte der Transformerclass des CWBW Addons von LabyStudio, also credits an ihn (!)
     * Hab den ganzen Tag damit verbracht das hier hinzubekommen, hatte es auch glaube ich recht weit.
     * Hab dann irgendwas verkackt das die ganze Zeit nh Exception kam und ja, kein bock mehr gehabt
     */

    /*
    *     You must add "transformerClass":"thisClassPath" to your addon.yml
    */

    /*
     *  The Class which contains the
     * `public static boolean packet(net.minecraft.network.Packet packtIn)` method
     */
    private final String methodClass = "de/minelucraft/rewinsideAddon/eventsystem/asm/Methods";

    public byte[] transform(String name, String transformedName, byte[] basicClass) {

        boolean obf = name.equals("fh");

        if (!obf && !name.equals("net.minecraft.network.PacketThreadUtil"))
            return basicClass;

        ClassNode classNode = new ClassNode();
        ClassReader classReader = new ClassReader(basicClass);
        classReader.accept(classNode, 0);
        Iterator iterator = classNode.methods.iterator();

        while (iterator.hasNext()) {
            MethodNode method = (MethodNode) iterator.next();
            if (method.name.equals("a") || method.name.equals("checkThreadAndEnqueue")) {
                AbstractInsnNode ifNode = null;
                AbstractInsnNode popNode = null;
                AbstractInsnNode[] nodes = method.instructions.toArray();

                for (int i = 0; i < nodes.length; ++i) {
                    AbstractInsnNode instruction = nodes[i];
                    if (ifNode == null && instruction.getOpcode() == 154)
                        ifNode = instruction;

                    if (popNode == null && instruction.getOpcode() == 87)
                        popNode = instruction;
                }

                InsnList insertList = new InsnList();

                insertList.add(new VarInsnNode(25, 0));
                insertList.add(new MethodInsnNode(184, methodClass, "packet", obf ? "(Lff;)Z" : "(Lnet/minecraft/network/Packet;)Z", false));

                LabelNode labelNode = new LabelNode();

                insertList.add(new JumpInsnNode(153, labelNode));

                method.instructions.insert(ifNode, insertList);
                method.instructions.insert(popNode, labelNode);
                break;
            }
        }

        ClassWriter classWriter = new ClassWriter(3);
        classNode.accept(classWriter);
        return classWriter.toByteArray();

    }

}
