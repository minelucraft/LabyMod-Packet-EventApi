package de.minelucraft.rewinsideAddon.eventsystem.utilities;

public abstract class Event {
    
    private String name;

    public Event(String name) { this.name = name; }

    public Event() { this(null); }

    public String getName() { return name != null ? name : this.getClass().getName(); }

}
