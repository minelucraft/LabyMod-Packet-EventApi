package de.minelucraft.rewinsideAddon.eventsystem.utilities;

public class EventSystem implements PacketListener {

    private static EventSystem instance;
    private EventManager eventManager;

    public EventSystem() {
        instance = this;
        this.eventManager = new EventManager();
        this.getEventManager().registerListener(instance);
    }

    public EventManager getEventManager() { return eventManager; }

}
