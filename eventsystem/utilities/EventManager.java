package de.minelucraft.rewinsideAddon.eventsystem.utilities;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EventManager {
    private Map<Class<?>, List<EventHandlerMethod>> eventHandlers;

    public EventManager() {
        this.eventHandlers = new HashMap<>();
    }

    public Event callEvent(Event event) {
        if (eventHandlers.containsKey(event.getClass())) {
            for (EventHandlerMethod method : eventHandlers.get(event.getClass())) {
                try {
                    method.run(event);
                } catch (Exception ex) {
                    System.out.println("Error while executing event " + event.getName() + " in listener " + method.getListener().getClass().getSimpleName() + ".");
                    System.out.println("Caused by " + ex.getCause().toString());
                    ex.printStackTrace();
                }
            }
        }
        return event;
    }

    private List<EventHandlerMethod> getEventHandlerMethods(PacketListener listener) {
        List<EventHandlerMethod> eventHandlers = new ArrayList<>();
        for (Method method : listener.getClass().getDeclaredMethods()) {
            PacketEventHandler eventHandler = method.getAnnotation(PacketEventHandler.class);
            if (eventHandler != null) {
                Class<?>[] params = method.getParameterTypes();
                if (params.length == 1) {
                    Class<?> clazz = params[0];
                    eventHandlers.add(new EventHandlerMethod(listener, method, clazz));
                }
            }
        }
        return eventHandlers;
    }

    public void registerListener(PacketListener listener) {
        List<EventHandlerMethod> eventHandlerMethods = this.getEventHandlerMethods(listener);
        for (EventHandlerMethod method : eventHandlerMethods) {
            List<EventHandlerMethod> currentEventMethods;
            if (eventHandlers.get(method.getEventClass()) != null) {
                currentEventMethods = eventHandlers.get(method.getEventClass());
            } else {
                currentEventMethods = new ArrayList<>();
            }
            currentEventMethods.add(method);
            eventHandlers.put(method.getEventClass(), currentEventMethods);
        }
    }

}

