package de.minelucraft.rewinsideAddon.eventsystem.utilities;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class EventHandlerMethod {

    private PacketListener listener;
    private Method method;
    private Class<?> eventClass;

    EventHandlerMethod(PacketListener listener, Method method, Class<?> eventClass) {
        this.listener = listener;
        this.method = method;
        this.eventClass = eventClass;
    }

    public void run(Event event) throws InvocationTargetException, IllegalAccessException {
        method.invoke(listener, event);
    }

    public Class<?> getEventClass() { return eventClass; }

    public PacketListener getListener() { return listener; }

}

